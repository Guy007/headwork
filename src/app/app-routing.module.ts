import { NgModule } from '@angular/core';
import { RouterModule , Routes } from '@angular/router';
import{LoginComponent} from './config/login/login.component';
import{AppComponent} from './app.component';
import { from } from 'rxjs';

const routes : Routes = [
  {
    path: '' , 
    component: AppComponent
  },
{
    path: 'login' , 
    component: LoginComponent
  }]
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  declarations: [],
   exports: [RouterModule]
})
export class AppRoutingModule { }
