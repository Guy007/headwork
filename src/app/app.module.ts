import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// For MDB Angular Free
//import { CarouselModule, WavesModule, ButtonsModule} from 'angular-bootstrap-md'
//import { CarouselModule } from 'angular4-carousel';
import { MatGridListModule, MatCardModule, MatMenuModule,
  MatIconModule, MatButtonModule, MatToolbarModule,
  MatSidenavModule, MatListModule, MatTableModule,
  MatPaginatorModule, MatSortModule, MatDialogModule, MatInputModule,
   MatFormFieldModule, MatOptionModule, MatSelectModule } from '@angular/material';
import { UserComponent } from './users/user/user.component';
import { LoginComponent } from './config/login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { AccueilComponent } from './public/accueil/accueil.component';
//import {  } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,
    MyNavComponent,
    UserComponent,
    LoginComponent,
    AccueilComponent
  ],
  imports: [
   /* CarouselModule, WavesModule, ButtonsModule,*/
    BrowserModule,
    MatGridListModule, MatCardModule, MatMenuModule,
    LayoutModule,
   // CarouselModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,MatTableModule,
  MatPaginatorModule, MatSortModule, MatDialogModule, MatInputModule,
   MatFormFieldModule, MatOptionModule, MatSelectModule,BrowserAnimationsModule, AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
